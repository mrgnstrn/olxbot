package com.mrgnstrn.olxbot.service.impl;

import com.mrgnstrn.olxbot.bot.message.OfferNotificationMessage;
import com.mrgnstrn.olxbot.entity.OlxOffer;
import com.mrgnstrn.olxbot.service.BotService;
import com.mrgnstrn.olxbot.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TelegramNotificationService implements NotificationService {

    @Autowired
    private BotService botService;

    @Override
    public void notify(OlxOffer olxOffer) {
        OfferNotificationMessage offerNotificationMessage = new OfferNotificationMessage();
        offerNotificationMessage.setTitle(olxOffer.getTitle());
        offerNotificationMessage.setPrice(olxOffer.getPrice());
        offerNotificationMessage.setLink(olxOffer.getLink());
        botService.sendOfferNotificationMessage(offerNotificationMessage);
    }
}
