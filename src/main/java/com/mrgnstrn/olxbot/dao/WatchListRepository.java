package com.mrgnstrn.olxbot.dao;

import com.mrgnstrn.olxbot.entity.WatchList;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WatchListRepository extends MongoRepository<WatchList, String> {

    List<WatchList> findAllByInitialIndexBuiltTrue();

}
