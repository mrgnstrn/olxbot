package com.mrgnstrn.olxbot.bot.message;

import com.mrgnstrn.olxbot.entity.WatchList;

import java.util.List;

public class MyWatchlistInfoMessage {
    private List<WatchList> watchList;

    public List<WatchList> getWatchList() {
        return watchList;
    }

    public void setWatchList(List<WatchList> watchList) {
        this.watchList = watchList;
    }
}
