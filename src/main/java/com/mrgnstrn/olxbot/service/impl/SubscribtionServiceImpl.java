package com.mrgnstrn.olxbot.service.impl;

import com.mrgnstrn.olxbot.dao.SubscriberRepository;
import com.mrgnstrn.olxbot.entity.Subscriber;
import com.mrgnstrn.olxbot.service.SubscriptionService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscribtionServiceImpl implements SubscriptionService {

    @Autowired
    private SubscriberRepository subscriberRepository;

    @Override
    public void subscribe(Subscriber subscriber) {
        subscriber.setId(ObjectId.get());
        subscriberRepository.save(subscriber);
    }
}
