package com.mrgnstrn.olxbot.bot.command.impl;

import com.mrgnstrn.olxbot.bot.command.Command;
import com.mrgnstrn.olxbot.entity.Subscriber;
import com.mrgnstrn.olxbot.service.BotService;
import com.mrgnstrn.olxbot.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

@Component
public class SubscribeCommand implements Command {
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private BotService botService;

    @Override
    public void perform(Message message) {
        Subscriber subscriber = new Subscriber();
        subscriber.setChatId(message.getChatId());
        subscriber.setUsername(message.getFrom().getUserName());
        subscriptionService.subscribe(subscriber);
        botService.sendSimpleMessage("Successfully subscribed");
    }
}
