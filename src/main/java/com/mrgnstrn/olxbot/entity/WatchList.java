package com.mrgnstrn.olxbot.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class WatchList {
    @Id
    private ObjectId id;
    @Indexed(unique = true)
    private String url;
    private String alias;
    private boolean initialIndexBuilt;

    public boolean isInitialIndexBuilt() {
        return initialIndexBuilt;
    }

    public void setInitialIndexBuilt(boolean initialIndexBuilt) {
        this.initialIndexBuilt = initialIndexBuilt;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
