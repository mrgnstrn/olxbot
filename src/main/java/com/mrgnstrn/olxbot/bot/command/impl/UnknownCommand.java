package com.mrgnstrn.olxbot.bot.command.impl;

import com.mrgnstrn.olxbot.bot.command.Command;
import com.mrgnstrn.olxbot.service.BotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

@Component
public class UnknownCommand implements Command {
    @Autowired
    private BotService botService;

    @Override
    public void perform(Message message) {
        botService.sendSimpleMessage("Unknown command");
    }
}
