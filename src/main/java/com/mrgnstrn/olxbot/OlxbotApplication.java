package com.mrgnstrn.olxbot;

import com.mrgnstrn.olxbot.service.BotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
@EnableScheduling
public class OlxbotApplication implements CommandLineRunner {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BotService botService;

    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(OlxbotApplication.class, args);
    }


    @Override
    public void run(String... strings) throws Exception {

    }
}
