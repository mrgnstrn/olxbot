package com.mrgnstrn.olxbot.olxclient;

import com.mrgnstrn.olxbot.entity.WatchList;
import com.mrgnstrn.olxbot.entity.OlxOffer;

import java.util.List;

public interface OlxClient {

    int getTotalOffers(WatchList request);

    List<OlxOffer> getOffers(WatchList request, int page);
}
