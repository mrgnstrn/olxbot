package com.mrgnstrn.olxbot.service;

import com.mrgnstrn.olxbot.entity.WatchList;

public interface OlxIndexerService {

    void buildInitialIndexForRequest(WatchList watchList);

    void updateIndexForRequest(WatchList watchList);

    boolean isBusy();
}
