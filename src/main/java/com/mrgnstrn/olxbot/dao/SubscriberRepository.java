package com.mrgnstrn.olxbot.dao;

import com.mrgnstrn.olxbot.entity.Subscriber;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SubscriberRepository extends MongoRepository<Subscriber, String> {

    Subscriber findByUsername(String username);

}
