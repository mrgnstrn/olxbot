package com.mrgnstrn.olxbot.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
public class OlxOffer {
    @Id
    private ObjectId id;
    @Indexed(unique = true)
    private String link;
    @Indexed(unique = true)
    private String olxId;
    private String title;
    private ObjectId watchlistId;
    private String price;
    private boolean notificationSend;
    private boolean notificationNeeded;
    private LocalDateTime dateParsed;
    private LocalDateTime dateCreated;

    public String getOlxId() {
        return olxId;
    }

    public void setOlxId(String olxId) {
        this.olxId = olxId;
    }

    public ObjectId getWatchlistId() {
        return watchlistId;
    }

    public void setWatchlistId(ObjectId watchlistId) {
        this.watchlistId = watchlistId;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public boolean isNotificationNeeded() {
        return notificationNeeded;
    }

    public void setNotificationNeeded(boolean notificationNeeded) {
        this.notificationNeeded = notificationNeeded;
    }

    public boolean isNotificationSend() {
        return notificationSend;
    }

    public void setNotificationSend(boolean notificationSend) {
        this.notificationSend = notificationSend;
    }

    public LocalDateTime getDateParsed() {
        return dateParsed;
    }

    public void setDateParsed(LocalDateTime dateParsed) {
        this.dateParsed = dateParsed;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "OlxOffer{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
