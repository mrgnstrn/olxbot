package com.mrgnstrn.olxbot.bot.command.impl;

import com.mrgnstrn.olxbot.bot.command.Command;
import com.mrgnstrn.olxbot.dao.WatchListRepository;
import com.mrgnstrn.olxbot.entity.WatchList;
import com.mrgnstrn.olxbot.service.BotService;
import com.mrgnstrn.olxbot.service.OlxIndexerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.util.List;

@Component
public class RunAllInitialIndexBuild implements Command {

    @Autowired
    private OlxIndexerService olxIndexerService;
    @Autowired
    private WatchListRepository watchListRepository;
    @Autowired
    private BotService botService;

    @Override
    public void perform(Message message) {
        if (!olxIndexerService.isBusy()) {
            List<WatchList> allRequests = watchListRepository.findAll();
            allRequests.forEach(olxIndexerService::buildInitialIndexForRequest);
            botService.sendSimpleMessage("Finished indexing");
        } else {
            botService.sendSimpleMessage("Indexer is busy now. Try later");
        }
    }
}
