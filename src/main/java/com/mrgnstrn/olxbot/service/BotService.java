package com.mrgnstrn.olxbot.service;

import com.mrgnstrn.olxbot.bot.message.OfferNotificationMessage;

public interface BotService {
    void sendOfferNotificationMessage(OfferNotificationMessage offerNotificationMessage);

    void sendSimpleMessage(String text);
}
