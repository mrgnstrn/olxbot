package com.mrgnstrn.olxbot.bot;

import com.mrgnstrn.olxbot.bot.command.Command;
import com.mrgnstrn.olxbot.bot.command.CommandResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

@Component
@PropertySource("classpath:botconfig.properties")
public class TelegramBot extends TelegramLongPollingBot {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Value("${bot.username}")
    private String BOT_USERNAME;
    @Value("${bot.token}")
    private String BOT_TOKEN;
    @Autowired
    private CommandResolver commandResolver;

    @Override
    public void onUpdateReceived(Update update) {
        if (hasMessage(update)) {
            Command command = commandResolver.resolve(update.getMessage());
            command.perform(update.getMessage());
        }
    }

    private boolean hasMessage(Update update) {
        return update.hasMessage() && update.getMessage().hasText();
    }

    @Override
    public String getBotUsername() {
        return BOT_USERNAME;
    }

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }

    public void sendMessage(long chatId, String message) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(message);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            LOG.error("Telegram exception occurred", e);
        }
    }
}
