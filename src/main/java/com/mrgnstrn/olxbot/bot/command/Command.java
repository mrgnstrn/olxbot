package com.mrgnstrn.olxbot.bot.command;

import org.telegram.telegrambots.api.objects.Message;

public interface Command {

    void perform(Message message);


}
