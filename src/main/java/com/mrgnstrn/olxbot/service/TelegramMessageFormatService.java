package com.mrgnstrn.olxbot.service;

import com.mrgnstrn.olxbot.bot.message.MyWatchlistInfoMessage;
import com.mrgnstrn.olxbot.bot.message.OfferNotificationMessage;

public interface TelegramMessageFormatService {
    String format(OfferNotificationMessage offerNotificationMessage);

    String format(MyWatchlistInfoMessage myWatchlistInfoMessage);
}
