package com.mrgnstrn.olxbot.cronjob;

import com.mrgnstrn.olxbot.dao.OlxOfferRepository;
import com.mrgnstrn.olxbot.entity.OlxOffer;
import com.mrgnstrn.olxbot.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NotificationJob {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private OlxOfferRepository olxOfferRepository;
    @Autowired
    private NotificationService notificationService;

    //TODO: Add logging and some results of performing task saving to db
    @Scheduled(cron = "${olxbot.notification.job.cron}")
    public void submitNotifications() {
        List<OlxOffer> unnotifiedOffers = olxOfferRepository.findByNotificationSendFalseAndNotificationNeededTrue();

        unnotifiedOffers.forEach(olxOffer -> {
            try {
                notificationService.notify(olxOffer);
                olxOffer.setNotificationSend(true);
                olxOfferRepository.save(olxOffer);
            } catch (Exception e) {
                LOG.error(String.format("Notification for offer [%s] was not send", olxOffer.getId()));
            }
        });

    }
}
