package com.mrgnstrn.olxbot.service;

import com.mrgnstrn.olxbot.entity.Subscriber;

public interface SubscriptionService {

    void subscribe(Subscriber subscriber);
}
