package com.mrgnstrn.olxbot.bot.command;

import com.mrgnstrn.olxbot.bot.command.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.objects.Message;

@Service
public class CommandResolver {
    @Autowired
    private SubscribeCommand subscribeCommand;
    @Autowired
    private UnknownCommand unknownCommand;
    @Autowired
    private RunAllUpdateIndexCommand runAllUpdateIndexCommand;
    @Autowired
    private RunAllInitialIndexBuild runAllInitialIndexBuild;
    @Autowired
    private GetMyWatchlistCommand getMyWatchlistCommand;
    @Autowired
    private AddWatchlistCommand addWatchlistCommand;

    public Command resolve(Message message) {
        String text = message.getText();

        if (text.contains("/subscribe")) {
            return subscribeCommand;
        } else if (text.contains("/update")) {
            return runAllUpdateIndexCommand;
        } else if (text.contains("/rebuild")) {
            return runAllInitialIndexBuild;
        } else if (text.contains("/watchlist")) {
            return getMyWatchlistCommand;
        } else if (text.contains("/addWatchlist")) {
            return addWatchlistCommand;
        }
        return unknownCommand;
    }
}
