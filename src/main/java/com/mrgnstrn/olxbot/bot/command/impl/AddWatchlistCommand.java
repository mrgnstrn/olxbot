package com.mrgnstrn.olxbot.bot.command.impl;

import com.mrgnstrn.olxbot.bot.command.Command;
import com.mrgnstrn.olxbot.bot.parsing.MessageParseService;
import com.mrgnstrn.olxbot.dao.WatchListRepository;
import com.mrgnstrn.olxbot.entity.WatchList;
import com.mrgnstrn.olxbot.service.BotService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.telegram.telegrambots.api.objects.Message;


@Component
public class AddWatchlistCommand implements Command {

    @Autowired
    private MessageParseService messageParseService;
    @Autowired
    private WatchListRepository watchListRepository;
    @Autowired
    private BotService botService;

    @Override
    public void perform(Message message) {
        String text = message.getText();
        String alias = messageParseService.extractFromTag("a", text);
        String url = messageParseService.extractFromTag("q", text);

        if (StringUtils.hasText(alias) && StringUtils.hasText(url)) {
            WatchList watchList = new WatchList();
            watchList.setId(ObjectId.get());
            watchList.setAlias(alias);
            watchList.setUrl(url);
            watchList.setInitialIndexBuilt(false);
            watchListRepository.save(watchList);
            botService.sendSimpleMessage("Added new watchlist");
        } else {
            botService.sendSimpleMessage("Something went wrong");
        }
    }
}
