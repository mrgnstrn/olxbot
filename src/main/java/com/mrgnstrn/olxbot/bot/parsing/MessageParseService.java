package com.mrgnstrn.olxbot.bot.parsing;


public interface MessageParseService {

    String extractFromTag(String tag, String text);
}
