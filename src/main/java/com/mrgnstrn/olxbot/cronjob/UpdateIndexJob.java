package com.mrgnstrn.olxbot.cronjob;

import com.mrgnstrn.olxbot.dao.WatchListRepository;
import com.mrgnstrn.olxbot.entity.WatchList;
import com.mrgnstrn.olxbot.service.OlxIndexerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UpdateIndexJob {

    @Autowired
    private OlxIndexerService olxIndexerService;
    @Autowired
    private WatchListRepository watchListRepository;

    //TODO: Add logging and some results of performing task saving to db
    @Scheduled(cron = "${olxbot.update.job.cron}")
    public void runUpdateIndex() {
        List<WatchList> watchLists = watchListRepository.findAllByInitialIndexBuiltTrue();
        watchLists.forEach(olxIndexerService::updateIndexForRequest);
    }

}
