
***This is app for parsing OLX.ua and sending notifications about new offers via Telegram.***  

First you need to register your bot and get bot token and username. Follow https://core.telegram.org/bots/faq#how-do-i-create-a-bot  

Install mongodb v3.6.2 and create db with name olxbot and adjust properties in \olxbot\src\main\resources\application.properties.      

Create botconfig.properties near application.properties with filled in (without {}):    

> bot.username={bot_username}    
> bot.token={bot_token}    
> bot.subscriber.username={your_telegram_username}   

Set your JAVA_HOME to point a latest jdk (1.8.0.152).    

Go to the project dir and perform and run "mvnw install" in command line.    
Grab generated .jar snapshot and run it with "java -jar olxbot.jar"    

Than first message to your bot must be /subscribe.  

Than you can perform next commands.  
/subscribe  
/addWatchlist a:"Alias for request" q:"/foto-video/obektivy/kharkov/". for example EXACTLY in that format. Url  
/rebuild  
/update  



**Then you must send a message from your account (with username as {your_telegram_username}) to bot with message "/subscribe" and receive confirmation.**  

TODO:  
- Rewrite readme LUL  
- add check for too big indexing. and slow down requests accordingly.  
- refactor code.  
- add help command with descriptions avoid dumb check.  
- for user existense add support for updating and rebuilding index for.    
- specific watchlist add validation for add watchlist functipnality add.   
- some tests LUL  
