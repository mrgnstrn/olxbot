package com.mrgnstrn.olxbot.dao;

import com.mrgnstrn.olxbot.entity.OlxOffer;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OlxOfferRepository extends MongoRepository<OlxOffer, String> {

    OlxOffer findByOlxIdAndWatchlistId(String olxId, ObjectId watchlistId);

    List<OlxOffer> findByNotificationSendFalseAndNotificationNeededTrue();

}
