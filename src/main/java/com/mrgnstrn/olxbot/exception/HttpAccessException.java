package com.mrgnstrn.olxbot.exception;

public class HttpAccessException extends RuntimeException {
    public HttpAccessException(String message) {
        super(message);
    }

    public HttpAccessException(String message, Throwable cause) {
        super(message, cause);
    }
}
