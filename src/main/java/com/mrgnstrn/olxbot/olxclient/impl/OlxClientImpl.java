package com.mrgnstrn.olxbot.olxclient.impl;

import com.mrgnstrn.olxbot.olxclient.OlxClient;
import com.mrgnstrn.olxbot.entity.OlxOffer;
import com.mrgnstrn.olxbot.entity.WatchList;
import com.mrgnstrn.olxbot.exception.HttpAccessException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class OlxClientImpl implements OlxClient {

    private static final String OLX_ROOT_URL = "https://www.olx.ua";

    @Override
    public int getTotalOffers(WatchList request) {
        try {
            String query = buildRequestQuery(request);
            Document document = Jsoup.connect(query).get();
            String text = document.getElementsByClass("dontHasPromoted").first().getElementsByTag("h2").first().text();
            String extractedNumber = deleteAllNonDigits(text);
            if (StringUtils.isEmpty(extractedNumber)) {
                return 0;
            } else {
                return Integer.valueOf(extractedNumber);
            }
        } catch (IOException e) {
            throw throwHttpAccessException(e);
        }
    }

    @Override
    public List<OlxOffer> getOffers(WatchList request, int page) {
        try {
            String query = buildRequestQuery(request, page);
            Document document = Jsoup.connect(query).get();
            Element offers_table = document.getElementById("offers_table");
            Elements offers = offers_table.getElementsByClass("offer");
            List<OlxOffer> olxOffers = new ArrayList<>();
            offers.forEach(offer -> {
                OlxOffer olxOffer = extractOfferDetails(offer);
                olxOffers.add(olxOffer);
            });
            return olxOffers;
        } catch (IOException e) {
            throw throwHttpAccessException(e);
        }
    }

    private OlxOffer extractOfferDetails(Element offer) {
        String title = extractTitle(offer);
        String link = extractLink(offer);
        String price = extractPrice(offer);
        String olxId = extractOlxId(offer);
        return createOffer(title, link, price, olxId);
    }

    private OlxOffer createOffer(String title, String link, String price, String olxId) {
        OlxOffer olxOffer = new OlxOffer();
        olxOffer.setTitle(title);
        olxOffer.setLink(link);
        olxOffer.setPrice(price);
        olxOffer.setOlxId(olxId);
        return olxOffer;
    }

    private String extractTitle(Element offer) {
        Element offerLinkElement = offer.getElementsByClass("marginright5 link linkWithHash detailsLink").first();
        return offerLinkElement.text();
    }

    private String extractLink(Element offer) {
        Element offerLinkElement = offer.getElementsByClass("marginright5 link linkWithHash detailsLink").first();
        return offerLinkElement.attr("href");

    }

    private String extractOlxId(Element offer) {
        Element childTable = offer.child(0);
        return childTable.attr("data-id");

    }

    private String extractPrice(Element offer) {
        Elements priceElement = offer.getElementsByClass("price");
        return priceElement.first().text();
    }

    private HttpAccessException throwHttpAccessException(IOException e) {
        return new HttpAccessException("Access to webpage was bot successful", e);
    }


    private String deleteAllNonDigits(String data) {
        data = data.replaceAll("\\D+", "");
        return data;
    }


    private String buildRequestQuery(WatchList watchList) {
        return OLX_ROOT_URL + watchList.getUrl();
    }

    private String buildRequestQuery(WatchList request, int page) {
        return OLX_ROOT_URL + request.getUrl() + "?page=" + page;
    }
}
