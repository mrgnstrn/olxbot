package com.mrgnstrn.olxbot.service.impl;

import com.mrgnstrn.olxbot.bot.message.MyWatchlistInfoMessage;
import com.mrgnstrn.olxbot.bot.message.OfferNotificationMessage;
import com.mrgnstrn.olxbot.service.TelegramMessageFormatService;
import org.springframework.stereotype.Service;

@Service
public class TelegramMessageFormatServiceImpl implements TelegramMessageFormatService {

    private static final String OFFER_NOTIFICATION_PATTERN = "Title: %s \nPrice: %s \nLink: %s";
    private static final String WATHCLIST_PATTERN = "You watchlist is: \n";

    @Override
    public String format(OfferNotificationMessage offerNotificationMessage) {
        return String.format(OFFER_NOTIFICATION_PATTERN, offerNotificationMessage.getTitle(), offerNotificationMessage.getPrice(), offerNotificationMessage.getLink());

    }

    @Override
    public String format(MyWatchlistInfoMessage myWatchlistInfoMessage) {
        StringBuilder sb = new StringBuilder();
        sb.append(WATHCLIST_PATTERN);
        myWatchlistInfoMessage.getWatchList().forEach(urlRequest -> {
            sb.append(urlRequest.getAlias()).append("\n");
            sb.append(urlRequest.getUrl()).append("\n");
        });
        sb.append("\n");
        return sb.toString();
    }
}
