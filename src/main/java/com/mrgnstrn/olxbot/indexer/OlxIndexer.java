package com.mrgnstrn.olxbot.indexer;

import com.mrgnstrn.olxbot.olxclient.OlxClient;
import com.mrgnstrn.olxbot.dao.OlxOfferRepository;
import com.mrgnstrn.olxbot.entity.OlxOffer;
import com.mrgnstrn.olxbot.entity.WatchList;
import com.mrgnstrn.olxbot.exception.IndexerStateException;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Component
public class OlxIndexer {
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    private boolean BUSY = false;
    private static final int OFFERS_PER_PAGE = 39;
    private static final int TIMEOUT = 2;
    @Autowired
    private OlxClient olxClient;
    @Autowired
    private OlxOfferRepository olxOfferRepository;


    public void buildIndex(WatchList watchList) {
        doInternallyWithLock(() -> {
            //THIS DELETES ALL.Be careful here when you will do selective build
            olxOfferRepository.deleteAll();
            try {
                doBuildIndex(watchList);
            } catch (InterruptedException e) {
                LOG.error("Thread was interrupted during TIMEOUT");
            }
        });
    }

    public void updateIndex(WatchList watchList) {
        doInternallyWithLock(() -> {
            try {
                doUpdateIndex(watchList);
            } catch (InterruptedException e) {
                LOG.error("Thread was interrupted during TIMEOUT");
            }
        });
    }

    private void doBuildIndex(WatchList watchList) throws InterruptedException {
        int totalOffers = olxClient.getTotalOffers(watchList);
        if (totalOffers <= 0) {
            return;
        }
        int totalPages = calculateTotalPagesCount(totalOffers);
        for (int page = 1; page <= totalPages; page++) {
            List<OlxOffer> offers = olxClient.getOffers(watchList, page);
            TimeUnit.SECONDS.sleep(TIMEOUT);
            offers.forEach(olxOffer -> {
                fillOlxOffer(olxOffer, watchList, false);
                saveOffer(olxOffer);
            });
        }
    }

    private void doUpdateIndex(WatchList watchList) throws InterruptedException {
        int totalOffers = olxClient.getTotalOffers(watchList);
        if (totalOffers <= 0) {
            return;
        }
        int totalPages = calculateTotalPagesCount(totalOffers);
        for (int page = 1; page <= totalPages; page++) {
            List<OlxOffer> offers = olxClient.getOffers(watchList, page);
            TimeUnit.SECONDS.sleep(TIMEOUT);
            for (OlxOffer olxOffer : offers) {
                OlxOffer offerById = olxOfferRepository.findByOlxIdAndWatchlistId(olxOffer.getOlxId(), watchList.getId());
                if (Objects.isNull(offerById)) {
                    fillOlxOffer(olxOffer, watchList, true);
                    saveOffer(olxOffer);
                } else {
                    return;
                }
            }
        }
    }

    public boolean isBusy() {
        return BUSY;
    }

    private void fillOlxOffer(OlxOffer olxOffer, WatchList watchList, boolean notificationNeeded) {
        olxOffer.setId(ObjectId.get());
        olxOffer.setDateParsed(LocalDateTime.now());
        olxOffer.setNotificationNeeded(notificationNeeded);
        olxOffer.setNotificationSend(false);
        olxOffer.setWatchlistId(watchList.getId());
    }

    private void saveOffer(OlxOffer olxOffer) {
        try {
            olxOfferRepository.save(olxOffer);
        } catch (DuplicateKeyException e) {
            LOG.error("Error saving offer [" + olxOffer + "]", e);
        }
    }


    private synchronized void doInternallyWithLock(Runnable runnable) {
        if (BUSY) {
            throw new IndexerStateException("Indexer is busy now, retry later");
        }
        BUSY = true;
        runnable.run();
        BUSY = false;
    }

    private int calculateTotalPagesCount(int totalOffers) {
        return (int) Math.ceil((double) totalOffers / OFFERS_PER_PAGE);
    }
}
