package com.mrgnstrn.olxbot.service.impl;

import com.mrgnstrn.olxbot.dao.WatchListRepository;
import com.mrgnstrn.olxbot.entity.WatchList;
import com.mrgnstrn.olxbot.indexer.OlxIndexer;
import com.mrgnstrn.olxbot.exception.IndexerStateException;
import com.mrgnstrn.olxbot.service.OlxIndexerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

@Service
public class OlxIndexerServiceImpl implements OlxIndexerService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private OlxIndexer olxIndexer;
    @Autowired
    private WatchListRepository watchListRepository;


    @Override
    public void buildInitialIndexForRequest(WatchList watchList) {
        checkIndexerState();
        olxIndexer.buildIndex(watchList);
        watchList.setInitialIndexBuilt(true);
        watchListRepository.save(watchList);
    }


    @Override
    public void updateIndexForRequest(WatchList watchList) {
        checkIndexerState();
        olxIndexer.updateIndex(watchList);
    }

    @Override
    public boolean isBusy() {
        return olxIndexer.isBusy();
    }

    private void checkIndexerState() {
        if (olxIndexer.isBusy()) {
            throw new IndexerStateException("Indexer is busy. Cant perform many indexes simultaneously.");
        }
    }

}
