package com.mrgnstrn.olxbot.bot.command.impl;

import com.mrgnstrn.olxbot.bot.command.Command;
import com.mrgnstrn.olxbot.bot.message.MyWatchlistInfoMessage;
import com.mrgnstrn.olxbot.dao.WatchListRepository;
import com.mrgnstrn.olxbot.entity.WatchList;
import com.mrgnstrn.olxbot.service.BotService;
import com.mrgnstrn.olxbot.service.TelegramMessageFormatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.objects.Message;

import java.util.List;

@Component
public class GetMyWatchlistCommand implements Command {

    @Autowired
    private WatchListRepository watchListRepository;
    @Autowired
    private TelegramMessageFormatService telegramMessageFormatService;
    @Autowired
    private BotService botService;

    @Override
    public void perform(Message message) {
        List<WatchList> all = watchListRepository.findAll();
        MyWatchlistInfoMessage watchlistInfoMessage = new MyWatchlistInfoMessage();
        watchlistInfoMessage.setWatchList(all);
        String formattedMessage = telegramMessageFormatService.format(watchlistInfoMessage);
        botService.sendSimpleMessage(formattedMessage);
    }
}
