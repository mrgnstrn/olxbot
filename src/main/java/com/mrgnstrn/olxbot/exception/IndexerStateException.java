package com.mrgnstrn.olxbot.exception;

public class IndexerStateException extends IllegalStateException {
    public IndexerStateException(String s) {
        super(s);
    }

    public IndexerStateException(String message, Throwable cause) {
        super(message, cause);
    }
}
