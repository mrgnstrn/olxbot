package com.mrgnstrn.olxbot.bot.parsing.impl;

import com.mrgnstrn.olxbot.bot.parsing.MessageParseService;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RegexMessageParseService implements MessageParseService {
    private static final String REGEX_PATTERN_TAG = "{%TAG}:\"(.*?)\"";

    @Override
    public String extractFromTag(String tag, String text) {
        String regex = REGEX_PATTERN_TAG.replace("{%TAG}", tag);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        boolean find = matcher.find();
        if (find) {
            return matcher.group(1);
        } else return "";
    }
}
