package com.mrgnstrn.olxbot.service;

import com.mrgnstrn.olxbot.entity.OlxOffer;

//TODO: Refactor and split logic of this service to support different channels of notification. This means that TelegramMessaging service should extend other interface
public interface NotificationService {

    void notify(OlxOffer olxOffer);
}
