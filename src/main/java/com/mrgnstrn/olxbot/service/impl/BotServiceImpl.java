package com.mrgnstrn.olxbot.service.impl;

import com.mrgnstrn.olxbot.bot.TelegramBot;
import com.mrgnstrn.olxbot.bot.message.OfferNotificationMessage;
import com.mrgnstrn.olxbot.dao.SubscriberRepository;
import com.mrgnstrn.olxbot.entity.Subscriber;
import com.mrgnstrn.olxbot.service.BotService;
import com.mrgnstrn.olxbot.service.TelegramMessageFormatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import javax.annotation.PostConstruct;

@Service
@PropertySource("classpath:botconfig.properties")
public class BotServiceImpl implements BotService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Value("${bot.subscriber.username}")
    private String onlyOneSubscriber;
    @Autowired
    private SubscriberRepository subscriberRepository;
    @Autowired
    private TelegramBot telegramBot;
    @Autowired
    private TelegramMessageFormatService telegramMessageFormatService;


    @PostConstruct
    public void init() {
        try {
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            telegramBotsApi.registerBot(telegramBot);
        } catch (TelegramApiRequestException e) {
            LOG.error("Telegram exception occurred", e);
        }
    }

    @Override
    public void sendOfferNotificationMessage(OfferNotificationMessage offerNotificationMessage) {
        Subscriber subscriber = subscriberRepository.findByUsername(onlyOneSubscriber);
        if (subscriber == null) {
            throw new IllegalStateException("Subscriber not defined");
        }

        telegramBot.sendMessage(subscriber.getChatId(), telegramMessageFormatService.format(offerNotificationMessage));
    }

    @Override
    public void sendSimpleMessage(String text) {
        Subscriber subscriber = subscriberRepository.findByUsername(onlyOneSubscriber);
        if (subscriber == null) {
            throw new IllegalStateException("Subscriber not defined");
        }
        telegramBot.sendMessage(subscriber.getChatId(), text);
    }

}
